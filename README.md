# dspl_sealite_driver_ros

ROS driver for one or more serial-controlled
[Deep Sea Power and Light](https://www.deepsea.com/)
[Sealite lights](https://www.deepsea.com/portfolio-items/led-sealite/) on a
single serial bus.

Built on [pydspl_seasense](https://gitlab.com/apl-ocean-engineering/pydspl_seasense)

The `dspl_sealite_driver` node takes the following parameters:

 * `~port`: File path or [URL](https://pyserial.readthedocs.io/en/latest/url_handlers.html)
 for the serial port.  `pydspl_seasense` is built on `pyserial`, so any
 `serial.serial_for_url()` should be valid.   Required, driver will not start
 if unset.

 * `~poll_interval_sec`:  Interval for driver polling of light status, in seconds.  If set
 to 0, polling is disabled.   Defaults to 5 seconds.

 * `~lights`: A map in the form of `{light_1: <addr1>, light_2: <addr2> }` which
 maps light names to Seasense addresses.   Light names must be unique.   A special name
 `all` is always defined which results in commands being sent to all lights ---
 this is done by looping through each light in the map *not* by using
 the Seasense protocol broadcast functionality.

The node will periodically poll all lights, collecting light status.
The results are published to the topic `sealite_status` with the
`SealiteStatus` message.   One `SealiteStatus` is broadcast for each light
at each poll interval.

## Scripts

The command line script `sealite_ctrl` generates and sends service calls to
the driver.

`sealite_ctrl level <level>` sets the light to `<level>`.

`sealite_ctrl level` or `sealite_ctrl off` sets the light level to 0 (off).

## Services

The node provides the following services:

### `SealiteLevel`:

Commands the node to set the level of the lights (turn them on/off/dim):

```
# lights is a list of light names or "all"
string[] lights
uint8 level           # In pct 0-100
---
int8 return_value      # 0 for success, negative for error
string message
```

`lights` is a list of comma-separated light names from the `~lights` parameter.
`level` is the integer level sent to each light given in the list.   `level`
is given in the native units used by Seasense, a percentage between 0 and 100,
inclusive.

`return_value` is 0 on success, negative on error.  In the event of an error,
`message` will contain a descriptive error message.

## Messages

The `SealiteStatus` message describes the current state of one light.  When
polling lights, the driver will send one `SealiteStatus` for each registered
light.

```
std_msgs/Header header
string light
int8 address
float32 temperature
int8 level
```

`light` is the name of the light, `address` is the Seasense address for the
light.  `temperature` is the reported temperature in degrees C, and `level`
is the current level (0-100).

The list of lights known to the driver can be introspected by observing the
set of `SealiteStatus` messages.
